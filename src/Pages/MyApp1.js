import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text,TouchableOpacity } from "react-native";


class MyApp1 extends Component {
  constructor (props){
    super (props);
    
    this.state = {
      firstname:"",
      lastname:"",
      fulltext:null
    };
  }

  
  firstnameTextChange = (inputText) =>{
    this.setState({firstname : inputText})
  }

  lastnameTextChange = (inputText) =>{
    this.setState({lastname : inputText})
  }
  updateFullText = () => {
    let text1 = this.state.firstname;
    let text2 = this.state.lastname;
    if (text1 !== null && text2 !== null && text1 !== ' ' && text2 !== ' ') {
      this.setState({fulltext: text1 + ' ' + text2});
    } else {
      Alert.alert('Info', 'Input tidak boleh kosong');
    }
  };
 
  render() {
    return (
      <SafeAreaView
      style={styles.halaman}>
        <Text style={styles.text}>
        Hello What's Your Name?</Text>
        <TextInput
          style={styles.input}
          onChangeText={this.firstnameTextChange}
          placeholder = "Your First Name"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.lastnameTextChange}
          placeholder = "Your Last Name"
        />
        <Text>  Hi My Name Is : {this.state.fulltext}</Text>
        <TouchableOpacity
          onPress={() => this.updateFullText()}
          style={styles.btn}>
          <Text>Save</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    backgroundColor:"white",
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  halaman :{
    height:900,
    backgroundColor:"pink",
    paddingVertical: 150,
  },
  btn: {
    backgroundColor: 'black',
    padding: 10,
    marginLeft:100,
    marginRight:100,
    justifyContent:"center",
    alignItems: 'center',
    borderRadius:5,
  },
  text: {
    fontSize: 40,
    paddingBottom:60,
    marginLeft: 40,
    marginTop:20,
    color: "black",
  },

});

export default MyApp1;